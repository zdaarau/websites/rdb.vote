#!/bin/bash
#
# Requirements:
#
# - CLI tools:
#   - [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) (obviously)
#   - [coreutils](https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands)
#   - [`curl`](https://curl.se/)
#   - [`git`](https://git-scm.com/)
#   - [`minify`](https://github.com/tdewolff/minify/tree/master/cmd/minify#readme)
#   - [`sd`](https://github.com/chmln/sd)

# ensure required CLI tools are available
if ! command -v curl >/dev/null ; then
  echo "\`curl\` is required but not found on PATH. See https://curl.se/download.html"
  exit 1
fi
if ! command -v git >/dev/null ; then
  echo "\`git\` is required but not found on PATH. See https://git-scm.com/downloads"
  exit 1
fi
if ! command -v minify >/dev/null ; then
  echo "\`minify\` is required but not found on PATH. See https://github.com/tdewolff/minify/tree/master/cmd/minify#readme"
  exit 1
fi
if ! command -v sd >/dev/null ; then
  echo "\`sd\` is required but not found on PATH. See https://github.com/chmln/sd#readme"
  exit 1
fi

# download and minify latest hypothes.is JS script
curl --location --silent https://hypothes.is/embed.js | minify --type=js | sd '\s*$' '\n' > public/hypothes-is.min.js

# stage, commit and push changes
git add public/hypothes-is.min.js
git commit -m "chore: update Hypothes.is JS script"
git push
