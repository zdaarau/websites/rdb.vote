# rdb.vote

[![Netlify Status](https://api.netlify.com/api/v1/badges/8c6b8c9a-58c9-485c-9316-93a42dd90f20/deploy-status)](https://app.netlify.com/sites/rdb-vote/deploys)

...

## Shared assets

### Counter.dev JS

To update the asset, run (from the root of the repo):

``` sh
/update_counter-dev.sh
```

### Hypothes.is JS

To update the asset, run (from the root of the repo):

``` sh
./update_hypothesis.sh
```

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).
